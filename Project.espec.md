# Mini-Projet

The Mini-Projet has to be developed in *groups of two students*.

## Datasets

List where data sets have to be chosen from:

- FR Data.gouv: https://www.data.gouv.fr/fr/
- Insee: https://www.insee.fr/fr/accueil
- Kaggle: https://www.kaggle.com/datasets
- US Data.gov: https://catalog.data.gov/dataset


## Guidelines

1. Justify the data set selection and described it
2. Formulate three questions about the selected data set
3. Discuss with the professors and choose one question among these
4. Propose a methodology to answer that question
5. Implement this methodology using Literate Programming
6. The report must be acessible online (in your git repository)

## Report

The R Markdown (to be written in RStudio) must contain:

- Frontpage
  - _Title_
  - Student's name
- Table of contents
- Introduction
  - Context / Dataset description
    - How the dataset has been obtained?
  - Description of the question
- Methodology
  - Data clean-up procedures
  - Scientific workflow
  - Data representation choices
- Analysis in Literate Programming
- Conclusion
- References

## Important notes

The question might be from simple to complex. More the question is
simple, more deep should be the analysis.

## Groups

Your project defence will be composed of (strictly) 5 minutes of presentation
and 2 minutes of questions. The presentation support could be anything: if you
have a PDF, you have to provide it to us in advance, if you need your own
machine be prepared (setup time is part of your defence time).

| id | Group | Time  | Teams                                        |   Repository
|----|-------|-------|----------------------------------------------|-------------------------
| 1  | G1    | 09:45 | Lakhyari Mohammed / Brunet Romain            | https://gricad-gitlab.univ-grenoble-alpes.fr/brunet2/MSPL
| 2  | G1    | 09:52 | Nebia Selma / Cuedari Imelda                 | https://gricad-gitlab.univ-grenoble-alpes.fr/nebias/MSPL
| 3  | G1    | 09:59 | Li Yi / Muwala Abel                          | https://gricad-gitlab.univ-grenoble-alpes.fr/li3/MSPL
| 4  | G1    | 10:06 | Aidara Sidi Mohamed / Bodiang Assane         | https://gricad-gitlab.univ-grenoble-alpes.fr/bodiangd/MSPL
| 5  | G1    | 10:13 | Benzaoui Mohamed / Benabbou Halima           | https://gricad-gitlab.univ-grenoble-alpes.fr/benzaoum/MSPL
| 6  | G1    | 10:20 | Fontaine Yves                                | https://gricad-gitlab.univ-grenoble-alpes.fr/fontaiyv/MSPL
| -  | -     | 10:27 | Pause                                        | -
| 7  | G1    | 10:34 | Bouillot Benjamin / Sirito-Olivier Roger     | https://gricad-gitlab.univ-grenoble-alpes.fr/bouillbe/MSPL
| 8  | G1    | 10:41 | Belabbes Pierre / Godont Pablo               | https://gricad-gitlab.univ-grenoble-alpes.fr/belabbep/MSPL
| 9  | G1    | 10:48 | Heresaz Clément / Vossier Maxime             | https://gricad-gitlab.univ-grenoble-alpes.fr/heresazc/MSPL
| 10 | G1    | 10:55 | Berger Pierre / Vincent Axel                 | https://gricad-gitlab.univ-grenoble-alpes.fr/vincenax/MSPL
| 11 | G1    | 11:02 | Chrétien Michel / Graver William             | https://gricad-gitlab.univ-grenoble-alpes.fr/chretimi/MSPL
| 12 | G1    | 11:09 | Masselin Thibaut / Dervieux Corentin         | https://gricad-gitlab.univ-grenoble-alpes.fr/masselit/MSPL
| -  | -     | 11:16 | Pause                                        | -
| 13 | G2    | 11:23 | Guillaume Metzger / Thomas Gevara            | https://gricad-gitlab.univ-grenoble-alpes.fr/metzgegu/MSPL
| 14 | G2    | 11:30 | Jules Noel / Gabriel Nourrit                 | https://gricad-gitlab.univ-grenoble-alpes.fr/noeljul/MSPL
| 15 | G2    | 11:37 | Julien Alaimo / Quentin Stehlin              | https://gricad-gitlab.univ-grenoble-alpes.fr/alaimoj/MSPL
| 16 | G2    | 11:44 | Mouataz Amal / Léo Bello                     | https://gricad-gitlab.univ-grenoble-alpes.fr/amalm/MSPL
| 17 | G2    | 11:51 | Tom Barthelemy / Seif Boubaker               | https://gricad-gitlab.univ-grenoble-alpes.fr/barthelt/MSPL
| 18 | G2    | 11:58 | Yanan Lyu / Jefferson Cessna                 | https://gricad-gitlab.univ-grenoble-alpes.fr/lyuya/MSPL
| 19 | G2    | 12:05 | Melanie Chasane / Céline Botté               | https://gricad-gitlab.univ-grenoble-alpes.fr/chasanem/MSPL
| -  | -     | 12:12 | Pause                                        | -
| 20 | G2    | 12:19 | Aminata Ba / Antoine Carrier                 | https://gricad-gitlab.univ-grenoble-alpes.fr/baa/MSPL
| 21 | G2    | 12:26 | Sébastien Lempreur / Rivana Alvano           | https://gricad-gitlab.univ-grenoble-alpes.fr/lemperes/MSPL
| 22 | G2    | 12:33 | Zakaria Choukchou Braham / Ilyas Bouyacoub   | https://gricad-gitlab.univ-grenoble-alpes.fr/choukchz/MSPL
| 23 | G2    | 12:40 | Zouhayr Aklouh / Amine Beluziane             | https://gricad-gitlab.univ-grenoble-alpes.fr/aklouhz/MSPL
| 24 | G2    | 12:47 | Jimmy Chevalier / Michael Koc                | https://gricad-gitlab.univ-grenoble-alpes.fr/chevajim/MSPL
| 25 | G2    | 12:54 | Noureddine Ziani / Abdelnour Sassi           | https://gricad-gitlab.univ-grenoble-alpes.fr/zianinou/MSPL
| 26 | G2    | 13:01 | Balle Traore / Loic Ramphort                 | https://gricad-gitlab.univ-grenoble-alpes.fr/traoreba/MSPL
| 27 | G2    | 13:08 | Diallo Thierno Ibrahima Sory/Diallo Mamadou Dian |  https://gricad-gitlab.univ-grenoble-alpes.fr/diallo1/Projet
|28|G2|17:10|Sultan Mourthadhoi| pas de git ?
|29|G1|17:17|Houssem Dahaoui / Nabil Aissat | https://gricad-gitlab.univ-grenoble-alpes.fr/dahaouih/MSPL 
|30|G1|17:24|Zahir loubna/El moussaoui Mohamed| git non spécifié
